﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVile.Models;
using System.IO;

namespace TooJaVile.Controllers
{
    [Authorize]
    public class AlbumsController : MyController
    {
        private TooJaVileEntities db = new TooJaVileEntities();

        // GET: Albums
        public ActionResult Index()
        {
            var albums = db.Albums.Include(a => a.Person);
            return View(albums.ToList());
        }

        // GET: Albums/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // GET: Albums/Create
        public ActionResult Create()
        {
            //Vaatame, kas on vaja
            //CheckPerson();
            //ViewBag.CurrentPerson = CurrentPerson;
            //ViewBag.CreatorId = CurrentPerson.Email;
            //ViewBag.FirstName = CurrentPerson.FirstName;
            //ViewBag.LastName = CurrentPerson.LastName;
            ViewBag.CoverPictureId = new SelectList(db.DataFiles, "Id", "ContentType");


            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email");
            return View();
        }

        // POST: Albums/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,CreatorId,Description,CoverPictureId,Created")] Album album, HttpPostedFileBase file)
        {
            //CheckPerson();
            //ViewBag.CurrentPerson = CurrentPerson;
            if (ModelState.IsValid)
            {
                
                    db.Albums.Add(album);
                    db.SaveChanges();


                //siia pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        
                        album.CoverPictureId = df.Id;
                        db.SaveChanges();
                       

                    }

                //siin lõpeb toimetamine pildiga


                return RedirectToAction("Index");
            }
            
            ViewBag.CoverPictureId = new SelectList(db.DataFiles, "Id", "ContentType", album.CoverPictureId);
            //ViewBag.CreatorId = CurrentPerson.Email;
            //ViewBag.FirstName = CurrentPerson.FirstName;
            //ViewBag.LastName = CurrentPerson.LastName;
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", album.CreatorId);
            return View(album);
        }

        // GET: Albums/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", album.CreatorId);
            return View(album);
        }

        // POST: Albums/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,CreatorId,Description,CoverPictureId,Created")] Album album, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(album).State = EntityState.Modified;
                db.Entry(album).Property("CoverPictureId").IsModified = false; //lisasin selle juurde, muidu kustub pilt ära
                db.SaveChanges();


                ChangeDataFile(
                               file,
                               x => { album.CoverPictureId = x; db.SaveChanges(); },
                                album.CoverPictureId);



                //siia pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        int? vanaPiltId = album.CoverPictureId;
                        album.CoverPictureId = df.Id;
                        db.SaveChanges();
                        //siin ei tohi ära kustutada pilte, sest meil on samu pilte mitmes kohas
                        //if (vanaPiltId.HasValue)
                        //{
                        //    db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                        //    db.SaveChanges();
                        //}
                    }

                //siin lõpeb toimetamine pildiga
                return RedirectToAction("Index");
            }
            ViewBag.CreatorId = new SelectList(db.People, "Id", "Email", album.CreatorId);
            return View(album);
        }

        // GET: Albums/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Album album = db.Albums.Find(id);
            if (album == null)
            {
                return HttpNotFound();
            }
            return View(album);
        }

        // POST: Albums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Album album = db.Albums.Find(id);
            db.Albums.Remove(album);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
