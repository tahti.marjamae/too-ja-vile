﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVile.Models;
using System.IO;

namespace TooJaVile.Controllers
{
    //public partial class Department
    //{
    //    //public static Dictionary<int, string> Departments = new Dictionary<int, string>
    //    //{
    //    //    {0, "Müük" },
    //    //    {1, "Ost" },
    //    //    {2, "Ladu" },
    //    //    {3, "Raamatupidamine" },
    //    //};
    //    //public string DepartmentName =>
    //    //    Departments.ContainsKey(Id) ? Departments[DepartmentNimi] : "";
    //}

    public class DepartmentsController : MyController
    {
        private TooJaVileEntities db = new TooJaVileEntities();

        // GET: Departments
        public ActionResult Index()
        {
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            var departments = db.Departments.Include(d => d.DataFile);
            return View(departments.ToList());
        }

        // GET: Departments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
        }


        // GET: Departments/Create
        public ActionResult Create()
        {
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
            return View();
        }

        // POST: Departments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,SortId,PictureId")] Department department, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Departments.Add(department);
                db.SaveChanges();

                //siia pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        int? vanaPiltId = department.PictureId;
                        department.PictureId = df.Id;
                        db.SaveChanges();
                        if (vanaPiltId.HasValue)
                        {
                            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                            db.SaveChanges();
                        }
                    }

                //siin lõpeb toimetamine pildiga

                return RedirectToAction("Index");
            }

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", department.PictureId);
            return View(department);
        }

        // GET: Departments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", department.PictureId);
            
            return View(department);
        }

        // POST: Departments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,SortId,PictureId")] Department department, HttpPostedFileBase file) //liia lisasin sulgudesse asju juurde
        {
            if (ModelState.IsValid)
            {
                db.Entry(department).State = EntityState.Modified;
                db.Entry(department).Property("PictureId").IsModified = false; //lisasin selle juurde, muidu kustub pilt ära
                db.SaveChanges();
                //siia pildiga toimetamine
                if(file != null && file.ContentLength > 0 )
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(), 
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        int? vanaPiltId = department.PictureId;
                        department.PictureId = df.Id;
                        db.SaveChanges();
                        if (vanaPiltId.HasValue)
                        {                            
                            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                            db.SaveChanges();
                        }
                    }

                        //siin lõpeb toimetamine pildiga
                        return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", department.PictureId);
            return View(department);
        }

        // GET: Departments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Department department = db.Departments.Find(id);
            db.Departments.Remove(department);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
