﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVile.Models;

namespace TooJaVile.Controllers
{
    public class DataFilesController : Controller
    {
        private TooJaVileEntities db = new TooJaVileEntities();

        // GET: DataFiles
        public ActionResult Index()
        {
            return View(db.DataFiles.ToList());
            
        }
        public ActionResult Content(int? id )       //siia lisasin faili lisamise asja. Piltide vaatamiseks on DataFiles 
        {
            DataFile df = db.DataFiles.Find(id ?? 0);
            if (db == null) return HttpNotFound();
            byte[] buff = df.Content;
            return File(buff, df.ContentType);      
        }

        // GET: DataFiles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // GET: DataFiles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: DataFiles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Content,ContentType,FileName,Created")] DataFile dataFile)
        {
            if (ModelState.IsValid)
            {
                db.DataFiles.Add(dataFile);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(dataFile);
        }

        // GET: DataFiles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // POST: DataFiles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Content,ContentType,FileName,Created")] DataFile dataFile)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dataFile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dataFile);
        }

        // GET: DataFiles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DataFile dataFile = db.DataFiles.Find(id);
            if (dataFile == null)
            {
                return HttpNotFound();
            }
            return View(dataFile);
        }

        // POST: DataFiles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DataFile dataFile = db.DataFiles.Find(id);
            db.DataFiles.Remove(dataFile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
