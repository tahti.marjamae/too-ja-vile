﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVile.Models;
using System.IO;

namespace TooJaVile.Controllers
{
    [Authorize]
    public class PhotosController : MyController
    {
        private TooJaVileEntities db = new TooJaVileEntities();

        // GET: Photos
        public ActionResult Index(int id = 0)
        {





            
            var photos = db.Photos.Include(p => p.DataFile).Include(p => p.Album)
                .Where(x => id == 0 || id == (x.AlbumId)); //lisasin, et leiaks albumvaates albumi nime järgi

            ViewBag.Ret = id;
            ViewBag.AlbumName = db.Albums.Find(id)?.Name ?? "";
            return View(photos.ToList());
        }

        // GET: Photos/Details/5
        public ActionResult Details(int? id, int ret = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ret = ret;
            return View(photo);
        }

        // GET: Photos/Create
        public ActionResult Create(int id = 0)
        {
            ViewBag.Ret = id;
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name");
            return View(new Photo { Created = DateTime.Now, PictureId = 0});
        }

        // POST: Photos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AlbumId,PictureId,Created")] Photo photo, HttpPostedFileBase file, int ret = 0)
        {
            if (ModelState.IsValid)
            {

                //siin ees ei ole db.Photos.Add(photo); ja db.SaveChanges();  kna pilt on kohustuslik ja ei saa enne salvestada, kui ta ei ole pilti lugenud. 
                //seetõttu on siin need asjad if-i sees, 

                //siia pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();                      
                        photo.PictureId = df.Id;
                        db.SaveChanges();
                        db.Photos.Add(photo);
                        db.SaveChanges();
                    }


                //siin lõpeb toimetamine pildiga
                return ret == 0
                    ? RedirectToAction("Index")
                    : RedirectToAction("Details", "Albums", new { id = ret })
                    ;
            }


            ViewBag.Ret = ret;
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", photo.PictureId);
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", photo.AlbumId);
            return View(photo);
        }

        // GET: Photos/Edit/5
        public ActionResult Edit(int? id, int ret = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ret = ret;
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", photo.PictureId);
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", photo.AlbumId);
            return View(photo);
        }

        // POST: Photos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AlbumId,PictureId,Created")] Photo photo, HttpPostedFileBase file, int ret = 0)
        {
            if (ModelState.IsValid)
            {
                db.Entry(photo).State = EntityState.Modified;
                db.Entry(photo).Property("PictureId").IsModified = false; //lisasin selle juurde, muidu kustub pilt ära
                db.SaveChanges();


                ChangeDataFile(
                               file,
                               x => { photo.PictureId = x; db.SaveChanges(); },
                                photo.PictureId);



                //siia pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        int? vanaPiltId = photo.PictureId;
                        photo.PictureId = df.Id;
                        db.SaveChanges();
                        //siin ei tohi vana pilti ära kustutada, sest meil on mitmes kohas sama pilt
                        //if (vanaPiltId.HasValue)
                        //{
                        //    db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                        //    db.SaveChanges();
                        //}
                    }

                //siin lõpeb toimetamine pildiga




                return ret == 0 
                    ? RedirectToAction("Index")
                    : RedirectToAction("Details", "Albums", new { id = ret})
                    ;
            }
            ViewBag.Ret = ret;
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", photo.PictureId);
            ViewBag.AlbumId = new SelectList(db.Albums, "Id", "Name", photo.AlbumId);
            return View(photo);
        }

        // GET: Photos/Delete/5
        public ActionResult Delete(int? id, int ret = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Photo photo = db.Photos.Find(id);
            if (photo == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ret = ret;
            return View(photo);
        }

        // POST: Photos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int ret = 0)
        {
            Photo photo = db.Photos.Find(id);
            db.Photos.Remove(photo);
            db.SaveChanges();
            return ret == 0
                    ? RedirectToAction("Index")
                    : RedirectToAction("Details", "Albums", new { id = ret })
                    ;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
