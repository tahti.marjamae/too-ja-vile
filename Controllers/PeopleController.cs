﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVile.Models;
using System.IO;


//lisasin siia ka namespace ja tegin partial classi, et meil oleks võimaluse korral asju teha, mis ei kustu ära
namespace TooJaVile.Models
{
 
    partial class Person
    {
        public static Person ByEmail(string email)
        {
            TooJaVileEntities db = new TooJaVileEntities();
            return db.People.Where(x => x.Email == email).SingleOrDefault();
        }
       
    }
}

namespace TooJaVile.Controllers
{
    [Authorize]
    public class PeopleController : MyController
    {
        private TooJaVileEntities db = new TooJaVileEntities();

        // GET: People
        public ActionResult Index(string sortOrder = "Name")
           
        {
            var inimesed = db.People.ToList();

            ViewBag.SortOrder = sortOrder;

            var persoon = from s in inimesed

                          select s;
            switch (sortOrder)
            {

                case "Name":
                    persoon = persoon.OrderBy(s => s.LastName);
                    break;
                case "Name_desc":
                    persoon = persoon.OrderByDescending(s => s.LastName);
                    break;
                case "Department":
                    persoon = persoon.OrderBy(s => s.Department.Name);
                    break;
                case "Department_desc":
                    persoon = persoon.OrderByDescending(s => s.Department.Name);
                    break; 
            }

                    //return View(persoon.ToList());


                    CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            if (CurrentPerson.IsAccountant) return View(persoon.ToList());
            if (CurrentPerson.IsManager) return View(CurrentPerson.Department.People.ToList());
            if (!CurrentPerson.IsAccountant && !CurrentPerson.IsManager) return View(CurrentPerson.Department.People.ToList());

            



            return RedirectToAction("People", "Index", new { id = CurrentPerson.Id });

        }
        public ActionResult VacationSummary(string sortOrder = "Name")
        {
            
            var inimesed = db.People.Include("Vacations").ToList();

            ViewBag.SortOrder = sortOrder;

            //CheckPerson();
            //ViewBag.CurrentPerson = CurrentPerson;


            var persoon = from s in inimesed
                            //where !Staatus.HasValue || s.VacationState == Staatus.Value
                            //where !Puhkuseliik.HasValue || s.VacationType == Puhkuseliik.Value
                            select s;
            switch (sortOrder)
            {

                case "Name":
                     persoon= persoon.OrderBy(s => s.LastName);
                    break;
                case "Name_desc":
                    persoon = persoon.OrderByDescending(s => s.LastName);
                    break;
                case "Department":
                    persoon = persoon.OrderBy(s => s.Department.Name);
                    break;
                case "Department_desc":
                    persoon = persoon.OrderByDescending(s => s.Department.Name);
                    break;
                    
            }

                    return View(persoon.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType");
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name");
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email");
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DisplayName,Email,FirstName,LastName,BirthDate,PictureId,DepartmentId,IsManager,IsAccountant,ParentId,IsStudent,VacationDays")] Person person, HttpPostedFileBase file)
        {
            

            //meiliaadress peab olema unikaalne
            if (person.Email!=""&& db.People.Where(x=>x.Email ==person.Email).Count()>0)


            {
                ModelState.AddModelError("Email", "Selline meiliaadress on juba registreeritud");

            }

            if (db.People.Where(x => x.FirstName == person.FirstName && person.LastName == x.LastName).Count() > 0)
            {

                ModelState.AddModelError("FirstName", "nimi kordub");
            }

            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();

                //siia pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        int? vanaPiltId = person.PictureId;
                        person.PictureId = df.Id;
                        db.SaveChanges();
                        if (vanaPiltId.HasValue)
                        {
                            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                            db.SaveChanges();
                        }
                    }

                //siin lõpeb toimetamine pildiga



                return RedirectToAction("Index");
            }

            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email", person.ParentId);
            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
           
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email", person.ParentId);
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //Alla sulgude lõppu lisasin pildi def.
        public ActionResult Edit([Bind(Include = "Id,Email,FirstName,LastName,BirthDate,PictureId,DepartmentId,IsManager,IsAccountant,ParentId,IsStudent,VacationDays")] Person person, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.Entry(person).Property("PictureId").IsModified = false; //lisasin selle juurde, muidu kustub pilt ära
                db.SaveChanges();

                ChangeDataFile(
                               file,
                               x => { person.PictureId = x; db.SaveChanges(); },
                               person.PictureId);



                //Siit algab pildiga toimetamine
                if (file != null && file.ContentLength > 0)
                    using (BinaryReader br = new BinaryReader(file.InputStream)) //see loeb pildifaili sisse
                    {
                        DataFile df = new DataFile
                        {
                            FileName = file.FileName.Split('\\').Last(),
                            ContentType = file.ContentType,
                            Content = br.ReadBytes(file.ContentLength),
                            Created = DateTime.Now,
                        };
                        db.DataFiles.Add(df);
                        db.SaveChanges();
                        //selleks et vana pilt kustutada, jätame meelde pildi numbri
                        int? vanaPiltId = person.PictureId;
                        person.PictureId = df.Id;
                        db.SaveChanges();
                        if (vanaPiltId.HasValue)
                        {
                            db.DataFiles.Remove(db.DataFiles.Find(vanaPiltId.Value));
                            db.SaveChanges();
                        }
                    }
                //siin lõpeb pildiga toimetamine




                return RedirectToAction("Index");
            }
            ViewBag.PictureId = new SelectList(db.DataFiles, "Id", "ContentType", person.PictureId);
            ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "Name", person.DepartmentId);
            ViewBag.ParentId = new SelectList(db.People, "Id", "Email", person.ParentId);
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
