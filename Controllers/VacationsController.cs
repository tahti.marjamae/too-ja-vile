﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TooJaVile.Models;
namespace TooJaVile.Models
{
   
    [Authorize]
    public partial class Vacation
    {
       


        public static Dictionary<int, string> VacationTypes = new Dictionary<int, string>
        {
           
            {0, "Põhipuhkus" },
            {1, "Palgata puhkus" },
            {2, "Õppepuhkus" },
            {3, "Lapsepuhkus" },
            {4, "Lapsehoolduspuhkus" },
            {5, "Isapuhkus" }

        };
        public static Dictionary<int, string> VacationStates = new Dictionary<int, string>
        {
            {0, "Esitatud" },
            {1, "Kinnitatud" },
            {2, "Tagasi lükatud" },
            {3, "Toimunud" },
            
        };

        public string VacationTypeName =>
            VacationTypes.ContainsKey(VacationType) ? VacationTypes[VacationType] : "";
            
    
        public string VacationStateName =>
            VacationStates.ContainsKey(VacationState) ? VacationStates[VacationState] : "";
        
    
    }
   
}


namespace TooJaVile.Controllers
{
    [Authorize]


    //SIIT JÄIME POOLELI
   
    public class VacationsController : MyController
    {
        private TooJaVileEntities db = new TooJaVileEntities();

        public static Dictionary<int, string> VacationTypes = new Dictionary<int, string>
        {

            {0, "Põhipuhkus" },
            {1, "Palgata puhkus" },
            {2, "Õppepuhkus" },
            {3, "Lapsepuhkus" },
            {4, "Lapsehoolduspuhkus" },
            {5, "Isapuhkus" }

        };
        
        // GET: Vacations
        public ActionResult Index(int? Staatus, int? Puhkuseliik, string sortOrder = "Name")

        {
            
            ViewBag.SortOrder = sortOrder;

            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;

            var vacation = CurrentPerson.IsAccountant ? db.Vacations.ToList()
                : db.Vacations.Where(x => x.Person.DepartmentId == CurrentPerson.DepartmentId).ToList();

            var vacations = from s in vacation
                           where !Staatus.HasValue|| s.VacationState == Staatus.Value
                            where !Puhkuseliik.HasValue || s.VacationType == Puhkuseliik.Value
                            select s;
            switch (sortOrder)
            {
                case "Date":
                    vacations = vacations.OrderBy(s => s.StartDate);
                    break;
                case "Date_desc":
                    vacations = vacations.OrderByDescending(s => s.StartDate);
                    break;
                case "EndDate":
                    vacations = vacations.OrderBy(s => s.EndDate);
                    break;
                case "EndDate_desc":
                    vacations = vacations.OrderByDescending(s => s.EndDate);
                    break;
                case "Name":
                    vacations = vacations.OrderBy(s => s.Person.LastName);
                    break;
                case "Name_desc":
                    vacations = vacations.OrderByDescending(s => s.Person.LastName);
                    break;
                case "Department":
                    vacations = vacations.OrderBy(s => s.Person.Department.Name);
                    break;
                case "Department_desc":
                    vacations = vacations.OrderByDescending(s => s.Person.Department.Name);
                    break;              
                case "Status":
                    vacations = vacations.OrderBy(s => s.VacationState);
                    break;
                case "Status_desc":
                    vacations = vacations.OrderByDescending(s => s.VacationState);
                    break;
                case "Type":
                    vacations = vacations.OrderBy(s => s.VacationType);
                    break;
                case "Type_desc":
                    vacations = vacations.OrderByDescending(s => s.VacationType);
                    break;
            }

            ViewBag.Staatus = new SelectList(Vacation.VacationStates, "Key", "Value", Staatus); //teeme dictionary listiks, siis saadame viewbagiga view-sse. 
            ViewBag.Puhkuseliik = new SelectList(Vacation.VacationTypes, "Key", "Value", Puhkuseliik); //teeme dictionary listiks, siis saadame viewbagiga view-sse. 
            
            return View(vacations.ToList());


            //var vacations = db.Vacations.Include(v => v.Person);
            //return View(vacations.ToList());

        }
        public ActionResult Calendar(int? month, int? year) //Lisasime Hennuga KL.
        {
            //var vacations = db.Vacations.Include(v => v.Person);
            //return View(vacations.ToList());
            /*private TooJaVileEntities db = new TooJaVileEntities();*/ // lisasin TooJaVileEntities KL
        //public ActionResult About(int? month, int? year) //lisatud sulgude sisu ja eemaldasin
        //{
            int monthv = month ?? DateTime.Today.Month;
            int yearv = year ?? DateTime.Today.Year;
            // kaks parameetrit, puudumisel võtame jooskva aasta ja/või jooksva kuu

            DateTime startdate = new DateTime(yearv, monthv, 1); // kuu algus
            DateTime calstart = startdate.AddDays(-(((int)startdate.DayOfWeek + 6) % 7));
            // mis kuupäevast peaks algama kalender 

            // kõik 4 paneme viewsse kaasa
            ViewBag.StartDate = startdate;
            ViewBag.CalStart = calstart;
            ViewBag.Month = monthv;
            ViewBag.Year = yearv;
            // Nüüd teeme kuus nädalat kuupäevi
            ViewBag.Dates = Enumerable
                .Range(0, 42)               // kuus nädalat (42)
                .GroupBy(x => x / 7)        // grupeerime 7 kaupa
                .Select(x => x.Select(y => calstart.AddDays(y)).ToList())
            // iga grupi teisendame kuupäevade listiks
            .ToList()       // ja kõik need grupid paneme omakorda listi
            ;
            // tulemuseks on umbes Datetime[6][7] massiiv, aga tegelikult List<List<DateTime>>

            // aga viewle mudeliks anname oma taskid - läheme nüüd seda viewd vaatama
            return View(db.Vacations
                //                .AsEnumerable()
                //                .Where(x => x.StartDate?.Year <= yearv && x.EndDate?.Year >= yearv)
                .ToList());




            //if(Request.IsAuthenticated)
            //{
            //    TooJaVileEntities db = new TooJaVile();
            //    ViewBag.DisplayName = User.Identity.Name;
            //    Person p = db.People.Where(x => x.EMail)
            //}
            /*return View();*/ /*kustutasin ja taastasin KL*/

        }
        public ActionResult Vacationdays(int? Staatus, int? Puhkuseliik, string sortOrder = "Name")

        {
            ViewBag.SortOrder = sortOrder;

            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            var vacation = CurrentPerson.IsAccountant ? db.Vacations.ToList()
                : db.Vacations.Where(x => x.Person.DepartmentId == CurrentPerson.DepartmentId).ToList();

            var vacations = from s in vacation
                            where !Staatus.HasValue || s.VacationState == Staatus.Value
                            where !Puhkuseliik.HasValue || s.VacationType == Puhkuseliik.Value
                            select s;
            switch (sortOrder)
            {
                
                case "Name":
                    vacations = vacations.OrderBy(s => s.Person.LastName);
                    break;
                case "Name_desc":
                    vacations = vacations.OrderByDescending(s => s.Person.LastName);
                    break;
                case "Department":
                    vacations = vacations.OrderBy(s => s.Person.Department.Name);
                    break;
                case "Department_desc":
                    vacations = vacations.OrderByDescending(s => s.Person.Department.Name);
                    break;
                
            }


            //Seda me ei kasuta siinkohal
            //var puhkusejäägid = db.People
            //    .Select(x => new
            //    {
            //        x.FirstName,
            //        Jääk = x.VacationDays - x.Vacations.AsEnumerable()
            //        .Where(y => y.VacationType == 0)
            //        .Where(y => y.StartDate.Value.Year == DateTime.Now.Year)
            //        .Sum(y => (y.EndDate.Value - y.StartDate.Value).Days)

            //    });

            return View(vacations.ToList());

        }
       

        public ActionResult Gallery()
        {
            return View(db.Vacations.ToList());

            
        }

        // GET: Vacations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            return View(vacation);
        }



        // GET: Vacations/Create
        public ActionResult Create(int? month, int? year)
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email");
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value");
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value");
            return View();
        }


        // POST: Vacations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonId,StartDate,EndDate,VacationState,VacationType")] Vacation vacation)
        {
            if (ModelState.IsValid)
            {
                //DateTime. uuspuhkus = vacation;

                //    db.Vacations.Add(vacation);

                if (vacation.StartDate > vacation.EndDate) /* = "End date must be greater than start date";*/
                                                           //throw  ("Kuupäevad ei klapi");

                    return RedirectToAction("Create");

                db.Vacations.Add(vacation);
                 db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", vacation.PersonId);
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value");
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value");
            return View(vacation);
        }

        // GET: Vacations/Edit/5
        public ActionResult Edit(int? id)
        {
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            if (id == null)
            {
               
               return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", vacation.PersonId);
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value", vacation.VacationType);
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value", vacation.VacationState);
            return View(vacation);
        }

        // POST: Vacations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonId,StartDate,EndDate,VacationState,VacationType")] Vacation vacation)
        {
            CheckPerson();
            ViewBag.CurrentPerson = CurrentPerson;
            if (ModelState.IsValid)
            {
                db.Entry(vacation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "Email", vacation.PersonId);
            ViewBag.VacationType = new SelectList(Vacation.VacationTypes, "Key", "Value", vacation.VacationType);
            ViewBag.VacationState = new SelectList(Vacation.VacationStates, "Key", "Value", vacation.VacationState);
            return View(vacation);
        }

        // GET: Vacations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vacation vacation = db.Vacations.Find(id);
            if (vacation == null)
            {
                return HttpNotFound();
            }
            return View(vacation);
        }

        // POST: Vacations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vacation vacation = db.Vacations.Find(id);
            db.Vacations.Remove(vacation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
